collect GRUB_DISTRIBUTOR sources:
- /etc/os-release/DISTRIB_DESCRIPTION= / $( lsb_release -ds )
- /etc/os-release/DISTRIB_ID= / $( lsb_release -is )
- /etc/lsb-release/DISTRIB_DESCRIPTION= / $( lsb_release -ds )
- /etc/lsb-release/DISTRIB_ID= / $( lsb_release -is )

