#!/bin/sh
# run grub-script-check (grub.cfg syntax checker) on all *.cfg files in current folder and subfolders (one level deep)


for i in *.cfg */*.cfg ; do   echo "  >> $i" ; grub-script-check $i  ; done

