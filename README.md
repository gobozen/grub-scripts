# grub-config-lib
A Grub2 runtime script library for easier grub.cfg customization
- changing config parameters like timeout, theme, background, gfxmode  without running update-grub (grub-mkconfig)

Grub binaries supported:
- vanilla grub-v2.02-beta2
  - Gentoo?, Arch, Manjaro
- debian's heavily patched grub-v2.02-beta2
- backward compatible with grub-v2.00  (todo: test)
  - Sabayon
- todo: test Burg

Distribution customizations supported:
- debian patches for grub-mkconfig
- recordfail -> $bootfailed_timeout=$menu_timeout=30 avoids headless server halt in bootmanager by default
  - http://ubuntuforums.org/archive/index.php/t-1703465.html
- quick_boot -> $quick_timeout=3 enabled after booting the default entry, revert to $menu_timeout=30 after choosing different entry
- vt_handoff -> bootparams: splash vt.handoff=7  go together if $gfxpayload==keep
  - todo: keep has to be disabled for some hardware (->hwmatch) and some trouble with efifb?

