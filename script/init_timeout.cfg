
###  timeout helper functions


# show the menu after finished loading
function set_show_menu {
  if  [ "$menu_timeout" ]  ;then  set timeout=$menu_timeout
  elif  [ ! "$timeout" ]  ;then  set timeout=60  ;fi
  set timeout_style=menu
}

# set $quick_timeout with $quick_timeout_style or $1
function set_quick_boot {
  if  [ "$quick_timeout" ]  ;then  set timeout=$quick_timeout
  else  set timeout=10  ;fi
  
  if  [ "$1" ]  ;then  set timeout_style=$1
  else  set timeout_style=$quick_timeout_style  ;fi
}

# instruct grub to boot immediately, without showing the menu
function set_immediate_timeout {
  set timeout=0
  unset timeout_style
}


# set timeout and timeout_style accordingly
function set_timeout {	# $timeout
  
  if  [ "$1" -eq 0  -a  "$1" != 0 ]  ;then  false ; return  ;fi

  if  [ "$limit_max_timeout" -ne 0 ]
  then
    if [ "$1" -lt 0  -o  "$1" -gt $limit_max_timeout ]  ;then  setparams $limit_max_timeout  ;fi
  fi

  set timeout=$1
  
  if [ 0 -lt "$1"  -a  "$1" -le "$quick_timeout" ]  ;then  set timeout_style=$quick_timeout_style
  #elif  [ 0 == "$1" ]  ;then  unset timeout_style
  else  set timeout_style=menu
  fi
  
}	# set_timeout



function set_timeout_preset {	# $timeout_preset  [<var name>]
  
  if    [ ! "$1" ]  ;then  false ; return  ;fi
  
  if    [ "$1" == '0' ]		;then  set_immediate_timeout
  elif  [ "$1" == 'immediate' ]	;then  set_immediate_timeout
  elif  [ "$1" == 'quick' ]	;then  set_quick_boot
  elif  [ "$1" == 'hidden' ]	;then  set_quick_boot hidden
  elif  [ "$1" == 'countdown' ]	;then  set_quick_boot countdown
  elif  [ "$1" == 'menu' ]	;then  set_show_menu
  # < 0 also valid: infinite  or  $limit_max_timeout
  elif  [ "$1" -ne 0 ]		;then  set_timeout $1
    # ad-hoc numeric value check: not equal 0 -> is true for non-zero numbers
    # and false for values not beginning with a number (parsed by grub as 0 value)
  else
    # if var_name provided then report invalid value
    if  [ "$2" ]  ;then  dbgEcho "invalid \$$2: '$1', set to: 'immediate'/'quick'/'countdown'/'hidden'/'menu'"
    fi
    false
  fi
}	# set_timeout_preset



# limit timeout for headless/server environments: $limit_max_timeout
# no hanging around in the bootmanager even after a boot-time powerloss
function limit_max_timeout {
  
  # if 0 < $limit_max_timeout (valid), sanity check would be  60 <  (1 minute)
  if  [ "$limit_max_timeout" -le 0 ]  ;then  false ; return  ;fi
  
  # sanitize $limit_max_timeout: >= menu_timeout  and  >= quick_timeout
  # $menu_timeout = ''  parses as value 0
  if  [ "$limit_max_timeout" -lt "$menu_timeout" ]
  then
    logWarn  "invalid setting: limit_max_timeout='$limit_max_timeout' less than menu_timeout='$menu_timeout', should be more"
    # explicit user set limit: takes precendence over potentionally generated timeout
    #limit_max_timeout=$menu_timeout
  fi
  if  [ "$limit_max_timeout" -lt "$quick_timeout" ]
  then
    logWarn  "invalid setting: limit_max_timeout='$limit_max_timeout' less than quick_timeout='$quick_timeout', should be more"
    # explicit user set limit: takes precendence over potentionally generated timeout
    #limit_max_timeout=$quick_timeout
  fi
  
  # if  $timeout is set and within limits  0 <=, <= $limit_max_timeout  then return
  if  [ "$timeout" -a  0 -le "$timeout"  -a  $timeout -le  $limit_max_timeout ) ]
  then  return  ;fi
  
  if  [ "$timeout_style"  -a  "$timeout_style"  !=  'menu'  -a  10 -lt $limit_max_timeout ]
  then
    logWarn  "limit_max_timeout:  timeout='$timeout', unsetting timeout_style '$timeout_style'"
    logWarn  "  long timeout would hang the system with almost no feedback to user"
    unset  timeout_style
  fi
  
  dbgEcho  "limit_max_timeout:  timeout='$timeout', setting to '$limit_max_timeout'"
  timeout=$limit_max_timeout
  
}	# limit_max_timeout




###  Before showing menu


# check for modifier keys to show menu: --shift --ctrl --alt
function check_keystatus {
  
  if  [ "$show_menu_keys" == '-' ]  ;then  return  ;fi
  # '-' disables this feature
  # by default all modifier keys will show the menu:  sometimes not all are available
  if  [ ! "$show_menu_keys" ]  ;then  set show_menu_keys='--shift --ctrl --alt'  ;fi
  if  keystatus
  then
    if  keystatus $show_menu_keys  ;then  set_show_menu  ;fi
    return
  fi
  
  if  debug_on
  then
    echo  "Ignoring show_menu_keys: none of the following terminal_input (s) support  keystatus $show_menu_keys"
    terminal_input
  fi
  if  [ "$timeout" -ge 0  -a  "$timeout" -lt 3 ]
  then
    dbgEcho  "Increasing \$timeout=$timeout to 3 seconds to give time to press ESC. Disable with show_menu_keys=-"
    set timeout=3
  fi
  return 2
  
}	# check_keystatus



# timeout before showing menu or booting default without showing menu
# 
# timeout_style compatibility:
# in grub-v2.00 the hidden/countdown timeout was implemented in script
# which has the nice feature being able to do things in script after the timeout
# and the drawback of not handling menuentry hotkeys: only ESC can open the menu
# 
# grub-v2.02 has builtin code for hidden timeout (feature_timeout_style=y) with hotkey handling
# drawback: it does not execute any script code after the hidden timeout (no callbacks available)
# set feature_timeout_style="n"
# if you want to do something after the hidden timeout, and handling hotkeys are not necessary

function wait_hidden_timeout {
  logFunc  wait_hidden_timeout
  
  check_keystatus	# shift/ctrl/alt ?

  # nothing to do if no $timeout, or negative (infinite)
  if  [ "$timeout" -le 0 ]  ;then  return  ;fi
  
  if  [ "$timeout_style" == countdown  -a  "$quick_timeout_message" ]
  then  echo -e  "$quick_timeout_message    "
  fi
  
  if  [ "$timeout_style" == countdown ]  ;then  _verbose='--verbose'
  elif  [ "$timeout_style" == hidden ]   ;then  _verbose=
  else  return			# default: show menu, then handle timeout
  fi
  
  # grub-v2.02 will handle it -- return false -> hidden timeout was not handled
  if  [ "$feature_timeout_style" = y ]  ;then  return 1  ;fi
  
  if  sleep $_verbose --interruptible $timeout
  then
    set timeout=0		# timeout expired -> boot instantly
    if  [ ! "$show_menu_keys" ]  ;then  true
    elif  keystatus $show_menu_keys  ;then  set_show_menu
    fi
  else  set_show_menu  ;fi	# enter menu -- user pressed ESC
  
  if  [ "$_verbose" ]  ;then  echo  ;fi
  unset _verbose		# delete local var
}


