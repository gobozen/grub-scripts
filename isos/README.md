Most LiveIso images use syslinux to boot
- boot options (kernel and params) configured in:  (loop)/isolinux/isolinux.cfg  or  boot/isolinux/isolinux.cfg
- simple to load with syslinux CONFIG directive as done by Yumi Multiboot Installer -> http://www.syslinux.org/wiki/index.php?title=Directives/config
  - iso contents are extracted to usb:  syslinux does not support loopback devices, nor any other than the boot device
- could be loaded with syslinux memdisk
  - loads the full iso to mem and chainloads it
  - no way to pass parameters to isolinux  ($iso_path -- location of iso file)
  - no way to alter kernel parameters to add $iso_path
- Grub2 supports loopback device, can load config and kernel from it
  - grub2 has builtin syslinux parser, but there's no way to alter kernel parameters to add $iso_path
  - parse commands in: http://git.savannah.gnu.org/cgit/grub.git/tree/grub-core/commands/syslinuxcfg.c#n184
    - syslinux_source  -> Execute syslinux config in same context
    - syslinux_configfile  -> Execute syslinux config in new context
    - extract_syslinux_entries_source  -> Execute syslinux config in same context taking only menu entries
    - extract_syslinux_entries_configfile  -> Execute syslinux config in new context taking only menu entries
- syslinux config can be executed as grub config script
  - necesssary syslinux directives implemented as functions
  -> grub-import-syslinux.cfg

story of  loopback.cfg, google results:
- http://www.supergrubdisk.org/wiki/Loopback.cfg
- http://www.normalesup.org/~george/comp/live_iso_usb/

distro bootparams to mount iso loopback (handled by initramfs/initrd)
- Arch: https://wiki.archlinux.org/index.php/Multiboot_USB_drive#Boot_entries
- Ubuntu:  https://help.ubuntu.com/community/Grub2/ISOBoot
- Gentoo:  https://wiki.gentoo.org/wiki/GRUB2/Chainloading#ISO_images
- https://gist.github.com/tactig/70cf264a9aff24822c53
- https://gist.github.com/lkraav/907965

distro bootparams in existing multiboot solutions
- https://github.com/mbusb/multibootusb/blob/master/scripts/update_cfg.py  --  many explicitly supported distro + generic + windows
  - isos are extracted into (usb)/multiboot/<iso>/, syslinux.cfg patched
- https://github.com/cyberorg/live-fat-stick/blob/master/live-fat-stick  --  only Fedora, openSuse, Ubuntu, LinuxMint

